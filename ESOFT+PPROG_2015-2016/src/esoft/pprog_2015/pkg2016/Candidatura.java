/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft.pprog_2015.pkg2016;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class Candidatura {
    
    private FAE fae;
    private Representante r;
    private String nome_empresa;
    private String morada;
    private int telemovel;
    private ArrayList<Produto> produtos;
    private ArrayList<Avaliacao> decisoes;
    private int num_convites;
   
    
    public Candidatura(){
        
    }
    
    public boolean valida(){
     
        return true;
    }
    public Avaliacao decidir(){
        return new Avaliacao();
    }

    /**
     * @return the fae
     */
    public FAE getFae() {
        return fae;
    }

    /**
     * @param fae the fae to set
     */
    public void setFae(FAE fae) {
        this.fae = fae;
    }

    /**
     * @return the r
     */
    public Representante getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(Representante r) {
        this.r = r;
    }

    /**
     * @return the nome_empresa
     */
    public String getNome_empresa() {
        return nome_empresa;
    }

    /**
     * @param nome_empresa the nome_empresa to set
     */
    public void setNome_empresa(String nome_empresa) {
        this.nome_empresa = nome_empresa;
    }

    /**
     * @return the morada
     */
    public String getMorada() {
        return morada;
    }

    /**
     * @param morada the morada to set
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * @return the telemovel
     */
    public int getTelemovel() {
        return telemovel;
    }

    /**
     * @param telemovel the telemovel to set
     */
    public void setTelemovel(int telemovel) {
        this.telemovel = telemovel;
    }

    /**
     * @return the produtos
     */
    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    /**
     * @param produtos the produtos to set
     */
    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    /**
     * @return the num_convites
     */
    public int getNum_convites() {
        return num_convites;
    }

    /**
     * @param num_convites the num_convites to set
     */
    public void setNum_convites(int num_convites) {
        this.num_convites = num_convites;
    }

    /**
     * @return the decisoes
     */
    public ArrayList<Avaliacao> getDecisoes() {
        return decisoes;
    }

    /**
     * @param decisoes the decisoes to set
     */
    public void setDecisoes(ArrayList<Avaliacao> decisoes) {
        this.decisoes = decisoes;
    }
    
}
