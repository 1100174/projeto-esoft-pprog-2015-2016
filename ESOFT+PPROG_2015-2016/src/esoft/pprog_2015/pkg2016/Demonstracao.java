/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft.pprog_2015.pkg2016;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class Demonstracao {
    
    private String descricao;
    private ArrayList<Recurso> recursos;
    
    public Demonstracao(){
        
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the recursos
     */
    public ArrayList<Recurso> getRecursos() {
        return recursos;
    }

    /**
     * @param recursos the recursos to set
     */
    public void setRecursos(ArrayList<Recurso> recursos) {
        this.recursos = recursos;
    }
    
}
