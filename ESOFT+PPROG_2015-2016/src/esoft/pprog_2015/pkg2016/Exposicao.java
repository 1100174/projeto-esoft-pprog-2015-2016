/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft.pprog_2015.pkg2016;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class Exposicao {
    
    private ArrayList<Organizador> organizadores;
    private ArrayList<FAE> lista_fae;
    private ArrayList<Candidatura> candidaturas;
    private ArrayList<Demonstracao> demos;
    
    
    public Exposicao(){
        
    }
    
    public void novaDemo(){
        
    }
    public Candidatura novaCandidatura(){
        
        return new Candidatura();
        
    }
    
    public void registarCandidatura(){
        
    }
    
    public boolean validaCandidatura(){
         return true;
    }

    /**
     * @return the organizadores
     */
    public ArrayList<Organizador> getOrganizadores() {
        return organizadores;
    }

    /**
     * @param organizadores the organizadores to set
     */
    public void setOrganizadores(ArrayList<Organizador> organizadores) {
        this.organizadores = organizadores;
    }

    /**
     * @return the lista_fae
     */
    public ArrayList<FAE> getLista_fae() {
        return lista_fae;
    }

    /**
     * @param lista_fae the lista_fae to set
     */
    public void setLista_fae(ArrayList<FAE> lista_fae) {
        this.lista_fae = lista_fae;
    }

    /**
     * @return the candidaturas
     */
    public ArrayList<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    /**
     * @param candidaturas the candidaturas to set
     */
    public void setCandidaturas(ArrayList<Candidatura> candidaturas) {
        this.candidaturas = candidaturas;
    }

    /**
     * @return the demos
     */
    public ArrayList<Demonstracao> getDemos() {
        return demos;
    }

    /**
     * @param demos the demos to set
     */
    public void setDemos(ArrayList<Demonstracao> demos) {
        this.demos = demos;
    }
}
