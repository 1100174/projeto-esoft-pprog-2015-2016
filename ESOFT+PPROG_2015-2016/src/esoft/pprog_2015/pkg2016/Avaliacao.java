/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft.pprog_2015.pkg2016;

/**
 *
 * @author Joao
 */
public class Avaliacao {
    
    private String texto_justificativo;
    private boolean decisao;
    
     public Avaliacao(){
        
    }
     
     public void valida(){
         
     }

    /**
     * @return the texto_justificativo
     */
    public String getTexto_justificativo() {
        return texto_justificativo;
    }

    /**
     * @param texto_justificativo the texto_justificativo to set
     */
    public void setTexto_justificativo(String texto_justificativo) {
        this.texto_justificativo = texto_justificativo;
    }

    /**
     * @return the decisao
     */
    public boolean isDecisao() {
        return decisao;
    }

    /**
     * @param decisao the decisao to set
     */
    public void setDecisao(boolean decisao) {
        this.decisao = decisao;
    }
}
