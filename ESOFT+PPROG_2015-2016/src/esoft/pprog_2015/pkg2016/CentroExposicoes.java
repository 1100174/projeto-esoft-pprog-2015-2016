/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esoft.pprog_2015.pkg2016;

import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class CentroExposicoes {
    
    private RegistoExposicoes registoE;
    private RegistoUtilizadores registoU;
    private ArrayList<Recurso> recursos;
    
    
    public CentroExposicoes(){
        
    }

    /**
     * @return the registoE
     */
    public RegistoExposicoes getRegistoE() {
        return registoE;
    }

    /**
     * @param registoE the registoE to set
     */
    public void setRegistoE(RegistoExposicoes registoE) {
        this.registoE = registoE;
    }

    /**
     * @return the registoU
     */
    public RegistoUtilizadores getRegistoU() {
        return registoU;
    }

    /**
     * @param registoU the registoU to set
     */
    public void setRegistoU(RegistoUtilizadores registoU) {
        this.registoU = registoU;
    }

    /**
     * @return the recursos
     */
    public ArrayList<Recurso> getRecursos() {
        return recursos;
    }

    /**
     * @param recursos the recursos to set
     */
    public void setRecursos(ArrayList<Recurso> recursos) {
        this.recursos = recursos;
    }
    
    public void novoRecurso(){
        
    }


    
}
