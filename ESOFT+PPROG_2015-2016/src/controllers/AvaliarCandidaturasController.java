/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import esoft.pprog_2015.pkg2016.Candidatura;
import esoft.pprog_2015.pkg2016.CentroExposicoes;
import esoft.pprog_2015.pkg2016.Avaliacao;
import esoft.pprog_2015.pkg2016.FAE;
import esoft.pprog_2015.pkg2016.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AvaliarCandidaturasController {
    
     private CentroExposicoes centro;
     private Candidatura c;
     private Avaliacao d;
     private ArrayList<Avaliacao> decisoes;
  

    
    public AvaliarCandidaturasController(CentroExposicoes c){
        this.centro=c;
        
    }
    
    public void addDecisao(){
        
    }
    public void valida(){}
   
    public ArrayList<Candidatura> getListaCandidaturas(Utilizador u){
        return getCentro().getRegistoE().getCandidaturas(u);
    }

    /**
     * @return the centro
     */
    public CentroExposicoes getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(CentroExposicoes centro) {
        this.centro = centro;
    }

    /**
     * @return the c
     */
    public Candidatura getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(Candidatura c) {
        this.c = c;
    }

    /**
     * @return the d
     */
    public Avaliacao getD() {
        return d;
    }

    /**
     * @param d the d to set
     */
    public void setD(Avaliacao d) {
        this.d = d;
    }

    /**
     * @return the decisoes
     */
    public ArrayList<Avaliacao> getDecisoes() {
        return decisoes;
    }

    /**
     * @param decisoes the decisoes to set
     */
    public void setDecisoes(ArrayList<Avaliacao> decisoes) {
        this.decisoes = decisoes;
    }
}
