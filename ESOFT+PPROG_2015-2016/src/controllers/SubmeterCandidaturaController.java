/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import esoft.pprog_2015.pkg2016.*;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class SubmeterCandidaturaController {
    private CentroExposicoes centro;
    private Exposicao e;
    private Candidatura candidatura;
    
    public SubmeterCandidaturaController(CentroExposicoes c){
        this.centro=c;
        
    }
    
    public void setDadosCandidatura(String dados){
        
    }
    public ArrayList<Exposicao> getListaExposicoes(){
        
        return getCentro().getRegistoE().getExposicoes();
    }

    /**
     * @return the centro
     */
    public CentroExposicoes getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(CentroExposicoes centro) {
        this.centro = centro;
    }

    /**
     * @return the e
     */
    public Exposicao getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(Exposicao e) {
        this.e = e;
    }

    /**
     * @return the candidatura
     */
    public Candidatura getCandidatura() {
        return candidatura;
    }

    /**
     * @param candidatura the candidatura to set
     */
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
}
