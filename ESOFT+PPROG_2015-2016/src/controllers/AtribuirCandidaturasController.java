/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import esoft.pprog_2015.pkg2016.Candidatura;
import esoft.pprog_2015.pkg2016.CentroExposicoes;
import esoft.pprog_2015.pkg2016.Exposicao;
import esoft.pprog_2015.pkg2016.FAE;
import esoft.pprog_2015.pkg2016.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class AtribuirCandidaturasController {
  private CentroExposicoes centro;
  private Exposicao e;
  private FAE f;
    
    public AtribuirCandidaturasController(CentroExposicoes c){
        this.centro=c;
        
    }
    public void valida(){}
   
    public ArrayList<Exposicao> getListaExposicoes(){
        return getCentro().getRegistoE().getExposicoes();
    }
    public ArrayList<FAE> getFAE(){
        return getE().getLista_fae();
    }
    

    public ArrayList<Candidatura> getListaCandidaturas(){
        return getE().getCandidaturas();
    }
    /**
     * @return the centro
     */
    public CentroExposicoes getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(CentroExposicoes centro) {
        this.centro = centro;
    }

    /**
     * @return the e
     */
    public Exposicao getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(Exposicao e) {
        this.e = e;
    }

    /**
     * @return the f
     */
    public FAE getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(FAE f) {
        this.f = f;
    }
}
