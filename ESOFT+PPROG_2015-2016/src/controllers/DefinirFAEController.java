/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import esoft.pprog_2015.pkg2016.CentroExposicoes;
import esoft.pprog_2015.pkg2016.Exposicao;
import esoft.pprog_2015.pkg2016.FAE;
import esoft.pprog_2015.pkg2016.Utilizador;
import java.util.ArrayList;

/**
 *
 * @author Joao
 */
public class DefinirFAEController {
    
     private CentroExposicoes centro;
    private Exposicao e;
    private ArrayList<FAE> fae;
    private FAE f;
  
    
    public DefinirFAEController(CentroExposicoes c){
        this.centro=c;
        
    }
    
    
    public void addFAE(Utilizador u){
        
    }
    
    public boolean valida(FAE f){
        
        return true;
        
    }
        public ArrayList<Exposicao> getListaExposicoes(){
        
        return getCentro().getRegistoE().getExposicoes();
    }
        
        
    /**
     * @return the centro
     */
    public CentroExposicoes getCentro() {
        return centro;
    }

    /**
     * @param centro the centro to set
     */
    public void setCentro(CentroExposicoes centro) {
        this.centro = centro;
    }

    /**
     * @return the e
     */
    public Exposicao getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(Exposicao e) {
        this.e = e;
    }

    /**
     * @return the f
     */
    public FAE getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(FAE f) {
        this.f = f;
    }

    /**
     * @return the fae
     */
    public ArrayList<FAE> getFae() {
        return fae;
    }

    /**
     * @param fae the fae to set
     */
    public void setFae(ArrayList<FAE> fae) {
        this.fae = fae;
    }
}
